﻿using System;

namespace PositiveOrNegative
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            int b;

            int max = int.MaxValue;
            int min = int.MinValue;

            Console.WriteLine(" \n Program - Positive or Negative v.0");

            Console.WriteLine(" Enter your first number: ");
            a = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(" Enter your second number: ");
            b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(" \n -------------------");

            if (a > max || a < min)
                Console.WriteLine(" Invalid first number.");
            else if (b > max || b < min)
                Console.WriteLine(" Invalid second number.");
            else if (a == 0 || b == 0)
                Console.WriteLine(" Result is null.");
            else if ((a > 0 && b > 0) || (a < 0 && b < 0))
                Console.WriteLine(" Result is positive.");
            else
                Console.WriteLine(" Result is negative.");

            Console.ReadKey();
        }
    }
}
