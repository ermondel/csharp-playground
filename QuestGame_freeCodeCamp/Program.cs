﻿using System;

namespace Lesson2903
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine(Cube(5));
            // Console.WriteLine(GetDayMyNumber(0));
            // Console.WriteLine(GetDayMyNumber(6));
            // Console.WriteLine(GetDayMyNumber(7));
            // MyLoop1();
            // MyLoop2();
            QuestGame("qwerty", 10);
            Console.ReadKey();
        }

        static void QuestGame(string secretWord, int steps)
        {
            bool play = true;
            bool win = false;
            string guess = "";

            while (play)
            {
                Console.Write("Enter quess: ");

                for (int step = 0; step < steps; step++)
                {
                    guess = Console.ReadLine();

                    if (guess == secretWord)
                    {
                        win = true;
                        step = steps;
                    }
                    else if (step < steps - 1)
                    {
                        Console.Write(" (" + (steps - 1 - step) + " left) try again: ");
                    }
                }

                if (win)
                {
                    Console.WriteLine("You Win!");

                    play = false;
                } else { 
                    Console.WriteLine("You lose.");
                    Console.Write("Do you want to play again? (yes/no): ");

                    string answer = Console.ReadLine();

                    if (answer != "yes")
                        play = false;
                }
            }

            Console.Write("Press any key to exit.");
        }

        static void MyLoop2()
        {
            string example = "Hello World";
            int index = 0;

            do
            {
                Console.WriteLine(" >> " + example[index]);
                index++;
            } while (index < example.Length);
        }

        static void MyLoop1()
        {
            int index = 0;

            while (index < 10)
            {
                Console.WriteLine(index);
                index++;
            }
        }

        static int Cube(int num)
        {
            int result = num * num * num;
            return result;
        }

        static string GetDayMyNumber(int dayNum)
        {
            string dayName = "";

            switch (dayNum)
            {
                case 0:
                    dayName = "Sunday";
                    break;
                case 1:
                    dayName = "Monday";
                    break;
                case 2:
                    dayName = "Tuesday";
                    break;
                case 3:
                    dayName = "Wednesaday";
                    break;
                case 4:
                    dayName = "Thursday";
                    break;
                case 5:
                    dayName = "Friday";
                    break;
                case 6:
                    dayName = "Saturday";
                    break;
            }

            return dayName;
        }
    }
}
