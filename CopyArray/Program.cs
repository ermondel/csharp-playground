﻿using System;

namespace CopyArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" \n Program - Copy Array v0.1 \n");

            int[] orig = new [] { 45, 67, 12, 34, 88, 38, 72, 5, 72, 78 };
            int[] copy = new int[orig.Length];

            for (int i = 0; i < orig.Length; i++)
            {
                copy[i] = orig[i];
                Console.WriteLine(" index[" + i + "]: " + orig[i] + " --> " + copy[i]);
            }

            Console.ReadKey();
        }
    }
}
