﻿using System;

namespace MonthsOfTheYear
{
    enum MonthsOfYear { 
        January = 1, 
        February = 2, 
        March = 3, 
        April = 4, 
        May = 5, 
        June = 6, 
        July = 7, 
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12
    }

    class Program
    {
        static void Main(string[] args)
        {
            int monthNumber;
            MonthsOfYear x;

            Console.WriteLine(" \n Program - Months of the Year v.0");

            Console.WriteLine(" \n Enter the month number: \n ");

            monthNumber = Convert.ToInt32(Console.ReadLine());

            if (monthNumber < 1 || monthNumber > 12)
            {
                Console.WriteLine(" \n Result: " + monthNumber + " is a wrong month number.");
                return;
            }

            switch ((MonthsOfYear)monthNumber)
            {
                case MonthsOfYear.January:
                    Console.WriteLine(" \n name January; order 1; number of days 31");
                    break;
                case MonthsOfYear.February:
                    Console.WriteLine(" \n name February; order 2; number of days 28/29");
                    break;
                case MonthsOfYear.March:
                    Console.WriteLine(" \n name March; order 3; number of days 31");
                    break;
                case MonthsOfYear.April:
                    Console.WriteLine(" \n name April; order 4; number of days 30");
                    break;
                case MonthsOfYear.May:
                    Console.WriteLine(" \n name May; order 5; number of days 31");
                    break;
                case MonthsOfYear.June:
                    Console.WriteLine(" \n name June; order 6; number of days 30");
                    break;
                case MonthsOfYear.July:
                    Console.WriteLine(" \n name July; order 7; number of days 31");
                    break;
                case MonthsOfYear.August:
                    Console.WriteLine(" \n name August; order 8; number of days 31");
                    break;
                case MonthsOfYear.September:
                    Console.WriteLine(" \n name September; order 9; number of days 30");
                    break;
                case MonthsOfYear.October:
                    Console.WriteLine(" \n name October; order 10; number of days 31");
                    break;
                case MonthsOfYear.November:
                    Console.WriteLine(" \n name November; order 11; number of days 30");
                    break;
                case MonthsOfYear.December:
                    Console.WriteLine(" \n name December; order 12; number of days 31");
                    break;
                default:
                    Console.WriteLine(" \n Error - unknown case.");
                    break;
            }

            Console.ReadKey();
        }
    }
}
