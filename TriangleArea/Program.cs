﻿using System;

namespace TriangleArea
{
    class Program
    {
        static void Main(string[] args)
        {
            // formula
            float triangleBase;
            float triangleHeight;
            float triangleArea;

            triangleBase = 1.5f;
            triangleHeight = 4;

            triangleArea = 0.5f * triangleBase * triangleHeight;

            Console.WriteLine(triangleArea);
        }
    }
}
