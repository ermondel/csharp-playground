﻿using System;

namespace DieRolling
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" \n Program - Die Rolling");
            Console.WriteLine(" | v 0.1");
            Console.WriteLine(" | enter 'q', 'quit' or 'exit' for exit");

            bool loop = true;

            string command;

            int numberOfDice;

            int resultOfThrow = 0;
            int totalResult = 0;

            Random random = new Random();

            while (loop)
            {
                // user input

                Console.WriteLine(" \n\n Enter the number of dice:");

                command = Console.ReadLine();

                // exit command

                if (command == "quit" || command == "q" || command == "exit") 
                { 
                    loop = false;

                    break;
                }

                // check input

                numberOfDice = Convert.ToInt32(command);

                if (numberOfDice <= 0)
                {
                    Console.WriteLine($" {numberOfDice} is a wrong number.");
                    continue;
                }


                // result

                Console.WriteLine($" \n Number of dices: {numberOfDice}");

                for (int i = 0; i < numberOfDice; i++)
                {
                    resultOfThrow = random.Next(6) + 1;

                    totalResult += resultOfThrow;

                    Console.WriteLine($" dice n{(i + 1)}, result: {resultOfThrow}");
                }

                Console.WriteLine($" ---------- \n Total: {totalResult}");

                // reset

                totalResult = 0;
            }

            // end
            Console.WriteLine(" \n Exit. Press any key.");
            Console.ReadKey();
        }
    }
}
