﻿using System;

namespace HelloWorld
{
    class Program
    {
        /* First demo programme */
        static void Main(string[] args)
        {
            // Console message
            Console.WriteLine("Message: I’m sorry, Dave. I’m afraid I can’t do that."); // This is also a comment.

            // Pause
            Console.ReadKey();
        }
    }
}
