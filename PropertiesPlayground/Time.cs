﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PropertiesPlayground
{
    class Time
    {
        private int seconds;

        public int Seconds
        {
            get
            {
                return seconds;
            }
            set
            {
                seconds = value;
            }
        }

        // public int Seconds { get; set; }

        public float Minutes
        {
            get
            {
                return ((float)seconds / 60);
            }
        }
    }
}
