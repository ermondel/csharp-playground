﻿using System;

namespace PropertiesPlayground
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n Program - Properties playground v0.1 \n");

            Player player = new Player();
            player.Score = 50;
            int currentScore = player.Score;
            Console.WriteLine(" " + currentScore + "\n");

            Time time = new Time();
            time.Seconds = 350;
            Console.WriteLine(" " + time.Seconds + "\n");
            Console.WriteLine(" " + time.Minutes + "\n");

            Vector2 vector2 = new Vector2(5.672, 8.891);
            Console.WriteLine(" " + vector2.X + "\n");
            Console.WriteLine(" " + vector2.Y + "\n");

            Book book = new Book("Frankenstein", "Mary Shelley");
            Console.WriteLine(" " + book.Title + "\n");
            Console.WriteLine(" " + book.Author + "\n");

            var anonymous = new { Name = "Steve", Age = 14 };
            Console.WriteLine($" {anonymous.Name} is {anonymous.Age} years old.");

            Console.ReadKey();
        }
    }
}
