﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PropertiesPlayground
{
    class Vector2
    {
        public double X { get; } = 0;
        public double Y { get; } = 0;

        public Vector2(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
