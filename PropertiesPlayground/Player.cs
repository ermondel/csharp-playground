﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PropertiesPlayground
{
    class Player
    {
        private int score;
        
        public int Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;

                if (score < 0)
                    score = 0;
            }
        }
    }
}
