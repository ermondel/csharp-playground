﻿using System;

namespace Cylinders
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputRadius;
            string inputHeight;

            double radius;
            double height;

            double volume;
            double surfaceArea;

            double pi = 3.141592654;

            // input

            Console.WriteLine(" \n Program: Cylinders v.0 ");
            Console.WriteLine(" ------------------------- ");

            Console.WriteLine(" \n Enter the cylinder's radius: ");
            inputRadius = Console.ReadLine();

            Console.WriteLine(" \n Enter cylinder height: ");
            inputHeight = Console.ReadLine();

            radius = Convert.ToDouble(inputRadius);
            height = Convert.ToDouble(inputHeight);

            // calc

            volume = pi * (radius * radius) * height;
            surfaceArea = 2 * pi * radius * (radius + height);

            // result

            Console.WriteLine(" \n | Result ");
            Console.WriteLine(" | The cylinder's volume is: " + volume + " cubic units.");
            Console.WriteLine(" | The cylinder's surface area is: " + surfaceArea + " square units.");

            Console.ReadKey();
        }
    }
}
