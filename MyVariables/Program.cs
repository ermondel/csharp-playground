﻿using System;

namespace MyVariables
{
    class Program
    {
        static void Main(string[] args)
        {
            byte    myByte    = 1;
            short   myShort   = 2;
            int     myInt     = 3;
            long    myLong    = 4;
            sbyte   mySByte   = 5;
            ushort  myUShort  = 6;
            uint    myUInt    = 7;
            ulong   myULong   = 8;
            char    myChar    = 'a';
            double  myDouble  = 9.99;
            float   myFloat   = 10.10f;
            decimal myDecimal = 11.11m;
            bool    myBool    = true;
            string  myString  = "abc";

            Console.WriteLine(myByte);
            Console.WriteLine(myShort);
            Console.WriteLine(myInt);
            Console.WriteLine(myLong);
            Console.WriteLine(mySByte);
            Console.WriteLine(myUShort);
            Console.WriteLine(myUInt);
            Console.WriteLine(myULong);
            Console.WriteLine(myChar);
            Console.WriteLine(myDouble);
            Console.WriteLine(myFloat);
            Console.WriteLine(myDecimal);
            Console.WriteLine(myBool);
            Console.WriteLine(myString);
        }
    }
}
