﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToe
{
    class Game
    {
        char[,] matrix;       // Game field
        char pin;             // X or O
        char winner;          // X or O
        byte fieldNumber;     // The number of the field selected by the player (1-9)
        bool turn1stPlayer;   // Indicates the current player
        string command;       // User command
        int score1stPlayer;   // 1st player score
        int score2ndPlayer;   // 2nd player score
        byte steps;           // Number of steps per round (usually 9)

        public Game()
        {
            this.matrix        = new char[3, 3];
            this.turn1stPlayer = true;
        }

        public void Play()
        {
            WriteDescription();

            while (true)
            {
                this.pin     = WritePlayerAndGetPin();
                this.command = Console.ReadLine();
                
                if (CommandExit())
                    break;

                this.fieldNumber = Convert.ToByte(this.command);

                if (!ValidFieldNumber()) 
                    continue;

                this.steps += 1;

                SetPinToField();

                WriteGameField();

                this.winner = WinCheck();

                if (this.winner != '\0')
                {
                    WriteWinner();

                    SetScore();
                    WriteScore();

                    if (!Replay())
                        break;

                    this.steps = 0;               // Reset step counter
                    this.matrix = new char[3, 3]; // New game field

                    WriteGameField();
                } else if (this.steps >= 9)
                {
                    WriteDraw();

                    if (!Replay())
                        break;

                    this.steps = 0;               // Reset step counter
                    this.matrix = new char[3, 3]; // New game field

                    WriteGameField();
                }
                
                this.turn1stPlayer = !this.turn1stPlayer; // Next player's turn
            }

            WriteExit();
        }

        private void WriteDescription()
        {
            Console.WriteLine(" \n Program - Tic-Tac-Toe");
            Console.WriteLine(" | v 1.0");
            Console.WriteLine(" | q, quit, exit - exit game");
            Console.WriteLine(" \n");
        }

        private void WriteGameField()
        {
            string row;

            Console.WriteLine("\n");

            for (int i = 0; i < this.matrix.GetLength(0); i++)
            {
                row = " ";

                for (int j = 0; j < this.matrix.GetLength(1); j++)
                {
                    if (this.matrix[i, j] == 'X')
                        row += " X ";
                    else if (this.matrix[i, j] == 'O')
                        row += " O ";
                    else
                        row += "   ";

                    if (j < this.matrix.GetLength(1) - 1)
                        row += "|";
                }

                Console.WriteLine(row);

                if (i < this.matrix.GetLength(0) - 1)
                    Console.WriteLine(" ---+---+---");
            }

            Console.WriteLine("\n");
        }

        private char WritePlayerAndGetPin()
        {
            if (this.turn1stPlayer)
            {
                Console.WriteLine(" 1st player (X): ");
                return 'X';
            }

            Console.WriteLine(" 2nd player (O): ");
            return 'O';
        }

        private void WriteExit()
        {
            Console.WriteLine(" \n | Exit. Press any key |");
            Console.ReadKey();
        }

        private void WriteWinner()
        {
            if (this.winner == 'X')
                Console.WriteLine(" 1st player is the winner!");
            else if (winner == 'O')
                Console.WriteLine(" 2nd player is the winner!");
            else
                Console.WriteLine(" Unknown player ");
        }

        private void WriteScore()
        {
            Console.WriteLine("\n");
            Console.WriteLine(" |-----------------");
            Console.WriteLine(" | Score           ");
            Console.WriteLine(" |-----------------");
            Console.WriteLine(" | 1st player : " + this.score1stPlayer);
            Console.WriteLine(" | 2nd player : " + this.score2ndPlayer);
            Console.WriteLine(" |-----------------");
            Console.WriteLine("\n");
        }

        private void WriteDraw()
        {
            Console.WriteLine(" Draw!");
        }

        private bool CommandExit()
        {
            if (this.command == "quit" || this.command == "q" || this.command == "exit")
                return true;
            else
                return false;
        }

        private bool ValidFieldNumber()
        {
            if (this.fieldNumber < 1 || this.fieldNumber > 9)
            {
                Console.WriteLine($"\n | {this.fieldNumber} is an invalid field number |\n");
                return false;
            }

            if (FieldCheck())
            {
                Console.WriteLine($"\n | Field {this.fieldNumber} is already taken |\n");
                return false;
            }

            return true;
        }

        private bool FieldCheck()
        {
            switch (this.fieldNumber)
            {
                case 1:
                    return this.matrix[2, 0] != '\0';

                case 2:
                    return this.matrix[2, 1] != '\0';

                case 3:
                    return this.matrix[2, 2] != '\0';

                case 4:
                    return this.matrix[1, 0] != '\0';

                case 5:
                    return this.matrix[1, 1] != '\0';

                case 6:
                    return this.matrix[1, 2] != '\0';

                case 7:
                    return this.matrix[0, 0] != '\0';

                case 8:
                    return this.matrix[0, 1] != '\0';

                case 9:
                    return this.matrix[0, 2] != '\0';

                default:
                    return true;
            }
        }

        private void SetPinToField()
        {
            switch (this.fieldNumber)
            {
                case 1:
                    this.matrix[2, 0] = this.pin;
                    break;

                case 2:
                    this.matrix[2, 1] = this.pin;
                    break;

                case 3:
                    this.matrix[2, 2] = this.pin;
                    break;

                case 4:
                    this.matrix[1, 0] = this.pin;
                    break;

                case 5:
                    this.matrix[1, 1] = this.pin;
                    break;

                case 6:
                    this.matrix[1, 2] = this.pin;
                    break;

                case 7:
                    this.matrix[0, 0] = this.pin;
                    break;

                case 8:
                    this.matrix[0, 1] = this.pin;
                    break;

                case 9:
                    this.matrix[0, 2] = this.pin;
                    break;

                default:
                    break;
            }
        }

        private bool Replay()
        {
            string command;

            while (true)
            {
                Console.WriteLine("\n | Play again? (yes/no) |");

                command = Console.ReadLine();

                if (command == "yes")
                    return true;
                else if (command == "no")
                    return false;
                else
                    Console.WriteLine($"\n | {command} - unknown command |");
            }
        }

        private void SetScore()
        {
            if (this.winner == 'X')
                this.score1stPlayer += 1;
            else if (winner == 'O')
                this.score2ndPlayer += 1;
            else
                Console.WriteLine(" Unknown player ");
        }

        private char WinCheck()
        {
            char result;

            result = WinCheckHorizontal();

            if (result != '\0')
                return result;

            result = WinCheckVertical();

            if (result != '\0')
                return result;

            result = WinCheckDiagonalLR();

            if (result != '\0')
                return result;

            result = WinCheckDiagonalRL();

            if (result != '\0')
                return result;

            return '\0';
        }

        private char WinCheckHorizontal()
        {
            string result = "";

            for (int row = 0; row < this.matrix.GetLength(0); row++)
            {
                for (int column = 0; column < this.matrix.GetLength(1); column++)
                    result += Convert.ToString(this.matrix[row, column]);

                if (result == "XXX")
                    return 'X';
                else if (result == "OOO")
                    return 'O';
                else
                    result = "";
            }

            return '\0';
        }

        private char WinCheckVertical()
        {
            string result = "";

            for (int row = 0; row < this.matrix.GetLength(1); row++)
            {
                for (byte column = 0; column < this.matrix.GetLength(0); column++)
                    result += Convert.ToString(this.matrix[column, row]);

                if (result == "XXX")
                    return 'X';
                else if (result == "OOO")
                    return 'O';
                else
                    result = "";
            }

            return '\0';
        }

        private char WinCheckDiagonalLR()
        {
            string result = "";

            for (int n = 0; n < this.matrix.GetLength(0); n++)
                result += Convert.ToString(this.matrix[n, n]);

            if (result == "XXX")
                return 'X';
            else if (result == "OOO")
                return 'O';
            else
                return '\0';
        }

        private char WinCheckDiagonalRL()
        {
            string result = "";

            int matrixLength = this.matrix.GetLength(0);

            for (int n = 0; n < matrixLength; n++)
                result += Convert.ToString(matrix[n, matrixLength - n - 1]);

            if (result == "XXX")
                return 'X';
            else if (result == "OOO")
                return 'O';
            else
                return '\0';
        }
    }
}
