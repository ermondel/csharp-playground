﻿using System;

namespace MakingGeneric
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" \n Program - Building Your Own Generic List Class, v0.1 \n ");

            // Creation of an array with a set of random values for the generic 

            Random random = new Random();

            int numberOfRandomValues = random.Next(20);
            int[] randomValues = new int[numberOfRandomValues];

            for (int i = 0; i < numberOfRandomValues; i++)
                randomValues[i] = random.Next(100);


            // Working with generic

            PracticeList<int> practiceListInt = new PracticeList<int>();

            for (int i = 0; i < randomValues.Length; i++)
                practiceListInt.Add(randomValues[i]);

            // Show result

            Console.WriteLine(" Result \n -----------------------------");
            Console.WriteLine(" Length: " + practiceListInt.GetLength() + "\n");

            for (int i = 0; i < practiceListInt.GetLength(); i++)
                Console.WriteLine($" index: {i}, value: {practiceListInt.GetItem(i)}");

            Console.ReadKey();
        }
    }
}
