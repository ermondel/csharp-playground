﻿using System;

namespace MyBook
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" \n Program - My Book, v.0 \n ");

            Book book = new Book("Harry Potter", "J.K. Rowling");

            book.SetTitle("Harry Potter and the Half-Blood Prince");

            Console.WriteLine(" " + book.GetTitle());

            Console.ReadKey();
        }
    }
}
