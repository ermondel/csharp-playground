﻿using System;

namespace MyRemainders
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            int b;
            int quotient;
            int remainder;
            int test;

            a = 102;
            b = 10;

            quotient = a / b;
            remainder = a % b;

            test = b * quotient + remainder;
            
            Console.WriteLine("\n Program v.0 \n");
            Console.WriteLine(" " + a + "/" + b + " is " + quotient + " remainder " + remainder);
            Console.WriteLine(" Test: " + test);
        }
    }
}
