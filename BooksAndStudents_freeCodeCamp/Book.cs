﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3103
{
    class Book
    {
        public string title;
        public string author;
        public int pages;

        public Book(string _title, string _author, int _pages)
        {
            title  = _title;
            author = _author;
            pages  = _pages;
        }
    }
}
