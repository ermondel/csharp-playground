﻿using System;

namespace Lesson3103
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Book book1 = new Book("Harry Potter", "JK Rowling", 400);
            Book book2 = new Book("The Lord of the Rings", "Tolkien", 1200);

            Console.WriteLine(book1.title);
            Console.WriteLine(book2.title);
            */

            Student student1 = new Student("Jim", "Business", 2.8);
            Student student2 = new Student("Pam", "Art", 3.6);

            Console.WriteLine(student1.HasHonors());
            Console.WriteLine(student2.HasHonors());

            Console.ReadKey();
        }
    }
}
