﻿using System;

namespace ReverseArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" \n Program - Reversing an Array, v0.1 \n ");

            int[] numbers = GenerateNumbers(15);
            Reverse(numbers);
            PrintNumbers(numbers);

            Console.ReadKey();
        }

        static int[] GenerateNumbers(int length)
        {
            int[] arr = new int[length];

            for (int i = 0; i < length; i++)
                arr[i] = i + 1;

            return arr;
        }

        static void PrintNumbers(int[] arr)
        {
            foreach (int n in arr)
                Console.WriteLine(" " + n);
        }

        static int[] Reverse(int[] arr)
        {
            int buff;
            int pairPosition;

            for (int n = 0; n < arr.Length / 2; n++)
            {
                buff = arr[n];
                pairPosition = arr.Length - n - 1;

                arr[n] = arr[pairPosition];
                arr[pairPosition] = buff;
            }

            return arr;
        }
    }
}
