﻿using System;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "";

            Console.WriteLine(" \n Program - FizzBuzz, v0.1 \n ");

            for (int i = 1; i <= 100; i++)
            {
                str += i % 3 == 0 ? "Fizz" : "";
                str += i % 5 == 0 ? "Buzz" : "";

                if (str != "")
                    Console.WriteLine(str);
                else
                    Console.WriteLine(i);

                str = "";
            }

            Console.ReadKey();
        }
    }
}
