﻿using System;

namespace Lesson0104
{
    class Program
    {
        static void Main(string[] args)
        {
            // Movies();
            // Songs();
            // UsefulTools.SayHi("Anon");

            Chef chef = new Chef();
            chef.MakeChicken();

            ItalianChef italianChef = new ItalianChef();
            italianChef.MakePasta();

            chef.MakeSpecialDish();
            italianChef.MakeSpecialDish();

            Console.ReadKey();
        }

        static void Songs()
        {
            Song holiday = new Song("Holiday", "Green Day", 200);

            Console.WriteLine(Song.songCount);
            Console.WriteLine(holiday.GetSongCount());

            Song kashmir = new Song("Kashmir", "Led Zeppelin", 150);

            Console.WriteLine(Song.songCount);
            Console.WriteLine(kashmir.GetSongCount());

            Song example = new Song("Some title", "Some author", 300);

            Console.WriteLine(Song.songCount);
            Console.WriteLine(example.GetSongCount());
        }

        static void Movies()
        {
            Console.WriteLine("=== Movies ===\n");

            Movie avengers = new Movie("The Avenger", "Joss Whedon", "PG-13");
            Movie shrek = new Movie("Shrek", "Adam Adamson", "PG");

            Console.WriteLine(avengers.title);
            Console.WriteLine(shrek.title);

            Console.WriteLine("\n");

            avengers.Rating = "Cat";
            shrek.Rating = "Dog";

            Console.WriteLine(avengers.Rating);
            Console.WriteLine(shrek.Rating);

            avengers.Rating = "G";
            shrek.Rating = "G";

            Console.WriteLine(avengers.Rating);
            Console.WriteLine(shrek.Rating);

            Console.WriteLine(avengers.CaseTest("123"));
            Console.WriteLine(shrek.CaseTest("qweqwe"));

            Console.WriteLine(avengers.CaseTest("AA"));
            Console.WriteLine(shrek.CaseTest("DD"));
        }
    }
}
