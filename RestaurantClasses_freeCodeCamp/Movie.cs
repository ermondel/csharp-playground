﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson0104
{
    class Movie
    {
        public string title;
        public string director;
        private string rating;

        public Movie(string _title, string _director, string _rating)
        {
            title    = _title;
            director = _director;
            Rating   = _rating;
        }

        public string Rating
        {
            get { return rating; }
            set
            {
                if (value == "G" ||
                    value == "PG" ||
                    value == "PG-13" || 
                    value == "R" ||
                    value == "NR")
                {
                    rating = value;
                } else
                {
                    rating = "NR";
                }
            }
        }

        public string CaseTest(string val)
        {
            string result = "";

            switch (val)
            {
                case "AA":
                case "BB":
                case "CC":
                case "DD":
                    result = val;
                    break;
                default:
                    result = "DEF";
                    break;
            }

            return result;
        }
    }
}
