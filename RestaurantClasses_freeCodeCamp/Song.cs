﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson0104
{
    class Song
    {
        public string title;
        public string artist;
        public int duration;
        public static int songCount = 0;

        public Song(string _title, string _artist, int _duration)
        {
            title    = _title;
            artist   = _artist;
            duration = _duration;

            songCount++;
        }

        public int GetSongCount()
        {
            return songCount;
        }
    }
}
