﻿using System;

namespace MinAndAverage
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" \n Program - Minimum and Averaging v0.1");

            // data

            int[] data = new int[] { 4, 51, -7, 13, -99, 15, -8, 45, 90 };

            // calculate - minimum

            int minimum = Int32.MaxValue;

            foreach (int n in data)
                if (n < minimum)
                    minimum = n;

            // calculate - average

            int total = 0;

            foreach (int n in data)
                total += n;

            float average = (float)total / data.Length;

            // result

            Console.Write(" \n Data: ");
            foreach (int n in data)
                Console.Write(n + ", ");

            Console.WriteLine(" \n \n Result");
            Console.WriteLine(" ---------------------------- ");
            Console.WriteLine(" Minimum: " + minimum);
            Console.WriteLine(" Average: " + average);
            
            Console.ReadKey();
        }
    }
}
