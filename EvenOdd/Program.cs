﻿using System;

namespace EvenOdd
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;
            string userInput;

            Console.WriteLine(" \n Program - Even and Odd v.0");
            Console.WriteLine(" Enter your number: ");

            userInput = Console.ReadLine();
            number = Convert.ToInt32(userInput);

            if (number % 2 == 0)
            {
                Console.WriteLine(" " + number + " is even");
            } else
            {
                Console.WriteLine(" " + number + " is odd");
            }

            Console.ReadKey();
        }
    }
}
