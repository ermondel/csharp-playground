﻿using System;

namespace TestCLIColor
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleColor nextBackgroundColor = ConsoleColor.Blue;
            ConsoleColor nextForegroundColor = ConsoleColor.White;

            ConsoleColor prevBackgroundColor = Console.BackgroundColor;
            ConsoleColor prevForegroundColor = Console.ForegroundColor;

            string inputData = "";

            bool runProgramme = true;

            //
            Console.BackgroundColor = nextBackgroundColor;
            Console.ForegroundColor = nextForegroundColor;

            Console.WriteLine(" === CLI programme === ");

            Console.BackgroundColor = prevBackgroundColor;
            Console.ForegroundColor = prevForegroundColor;

            while (runProgramme)
            {
                Console.Write("input: ");
                inputData = Console.ReadLine();

                if (inputData == "/exit")
                {
                    runProgramme = false;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine(inputData);
                }
            }

            Console.ReadKey();
        }
    }
}
