﻿using System;

namespace DesigningAndBuildingClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" \n Program - Designing and Building Classes, v0.1 \n ");

            // red ball 

            Ball redBall = new Ball(100, new Color(255, 0, 0));

            Console.WriteLine(" Red ball info \n -----------------");
            Console.WriteLine(" size: " + redBall.GetSize());
            Console.WriteLine(" color: " + redBall.GetColor());
            Console.WriteLine(" throws: " + redBall.GetThrows());
            Console.WriteLine("\n");

            redBall.Throw();
            redBall.Throw();
            redBall.Throw();
            redBall.Pop();
            redBall.Throw();
            redBall.Throw();

            Console.WriteLine(" throws: " + redBall.GetThrows() + "\n");

            // green ball 

            Ball greenBall = new Ball(180, new Color(0, 255, 0));

            Console.WriteLine(" Green ball info \n -----------------");
            Console.WriteLine(" size: " + greenBall.GetSize());
            Console.WriteLine(" color: " + greenBall.GetColor());
            Console.WriteLine(" throws: " + greenBall.GetThrows());
            Console.WriteLine("\n");

            greenBall.Throw();
            greenBall.Throw();
            greenBall.Throw();
            greenBall.Throw();
            greenBall.Throw();
            greenBall.Pop();
            greenBall.Throw();

            Console.WriteLine(" throws: " + greenBall.GetThrows() + "\n");

            // blue ball 

            Ball blueBall = new Ball(75, new Color(0, 0, 255));

            Console.WriteLine(" Blue ball info \n -----------------");
            Console.WriteLine(" size: " + blueBall.GetSize());
            Console.WriteLine(" color: " + blueBall.GetColor());
            Console.WriteLine(" throws: " + blueBall.GetThrows());
            Console.WriteLine("\n");

            blueBall.Throw();
            blueBall.Throw();

            Console.WriteLine(" throws: " + blueBall.GetThrows() + "\n");

            blueBall.Throw();
            blueBall.Throw();

            Console.WriteLine(" throws: " + blueBall.GetThrows() + "\n");

            // end

            Console.ReadKey();
        }
    }
}
