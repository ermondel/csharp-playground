﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesigningAndBuildingClasses
{
    class Color
    {
        private byte red;
        private byte green;
        private byte blue;
        private byte alpha = 255;

        public Color(byte red, byte green, byte blue)
        {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

        // get

        public byte GetRed()
        {
            return red;
        }

        public byte GetGreen()
        {
            return green;
        }

        public byte GetBlue()
        {
            return blue;
        }

        public byte GetAlpha()
        {
            return alpha;
        }

        // set

        public void SetRed(byte red)
        {
            this.red = red;
        }

        public void SetGreen(byte green)
        {
            this.green = green;
        }

        public void SetBlue(byte blue)
        {
            this.blue = blue;
        }

        public void SetAlpha(byte alpha)
        {
            this.alpha = alpha;
        }

        // methods

        public int grayscale()
        {
            return (this.red + this.green + this.blue) / 3;
        }

        public string rgba()
        {
            return $"({this.red}, {this.green}, {this.blue}, {this.alpha})";
        }
    }
}
