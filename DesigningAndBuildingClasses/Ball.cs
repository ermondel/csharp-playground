﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesigningAndBuildingClasses
{
    class Ball
    {
        private int size;
        private Color color;
        private int throws;

        public Ball(int size, Color color)
        {
            this.size = size;
            this.color = color;
        }

        // get

        public int GetSize()
        {
            return this.size;
        }

        public string GetColor()
        {
            return this.color.rgba();
        }

        public int GetThrows()
        {
            return this.throws;
        }

        // methods

        public void Pop()
        {
            this.size = 0;
        }

        public void Throw()
        {
            if (this.size > 0)
                ++this.throws;
        }
    }
}
