﻿using System;

namespace Lesson2703
{
    class Program
    {
        static void Main(string[] args)
        {
            // Example1();
            // Example2();
            // Example3();
            // Example4();
            SayHi("Paris", 795);

            Console.ReadLine();
        }

        static void SayHi(string name, int age)
        {
            Console.WriteLine("Hello " + name + ", you are " + age + ".");
        }

        static void Example4()
        {
            int[] luckyNumbers = { 4, 8, 15, 16, 23, 42 };

            string[] friends = new string[5];

            friends[0] = "Jim";
            friends[1] = "Kelly";

            luckyNumbers[0] = 444;

            Console.WriteLine(luckyNumbers[0]);
            Console.WriteLine(friends[0]);
        }

        static void Example3()
        {
            Console.Write("Enter a number: ");
            double num1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Enter another number: ");
            double num2 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine(" " + (num1 + num2));
        }

        static void Example2()
        {
            Console.Write("Enter your name: ");
            string name = Console.ReadLine();
            Console.Write("Enter your age: ");
            string age = Console.ReadLine();
            Console.WriteLine("Hello " + name + " you are " + age);
        }

        static void Example1()
        {
            string head = "My program\n v1 21/03/2022\n";
            string phrase = "Giraffe Academy" + " is cool";
            char grade = 'A';
            int age = 30;
            double gpa = 3.3;
            bool man = true;

            Console.WriteLine(" " + head);
            Console.WriteLine(" " + phrase);
            Console.WriteLine("     " + phrase.Length);
            Console.WriteLine("     " + phrase.ToUpper());
            Console.WriteLine("     " + phrase.ToLower());
            Console.WriteLine("     " + phrase.Contains("Academy"));
            Console.WriteLine("     " + phrase[0]);
            Console.WriteLine("     " + phrase.IndexOf("Academy"));
            Console.WriteLine("     " + phrase.IndexOf("w"));
            Console.WriteLine("     " + phrase.Substring(3, 4));
            Console.WriteLine(" " + grade);
            Console.WriteLine(" " + age);
            Console.WriteLine(" " + gpa);
            Console.WriteLine(" " + man);
            Console.WriteLine(" " + (5 / 2));
            Console.WriteLine(" " + (5 / 2.0));
            Console.WriteLine(" " + Math.Abs(-15.5));
            Console.WriteLine(" " + Math.Pow(3, 3));
            Console.WriteLine(" " + Math.Sqrt(36));
            Console.WriteLine(" " + Math.Max(67, 18));

        }
    }
}
