﻿using System;

namespace PrintPyramid
{
    class Program
    {
        static void Main(string[] args)
        {
            int linesNumber = 5;

            Console.WriteLine(" \n Program - Print a Pyramid, v0.1 \n ");

            for (int i = 0; i < linesNumber; i++)
            {
                for (int spaces = 0; spaces < (linesNumber - i); spaces++)
                    Console.Write(" ");

                for (int stars = 0; stars < (2 * i + 1); stars++)
                    Console.Write("*");

                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
