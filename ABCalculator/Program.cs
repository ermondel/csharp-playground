﻿using System;

namespace ABCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            float a;
            float b;
            char operation;

            // user inputs

            Console.WriteLine(" \n Program - AB calcualtor v.0");

            Console.WriteLine(" \n Enter the first value (A): ");
            a = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine(" \n A = " + a);

            Console.WriteLine(" \n Enter the second value (B): ");
            b = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine(" \n B = " + b);

            Console.WriteLine(" \n Enter the operation (+ - * / % ^): ");
            operation = Convert.ToChar(Console.ReadLine());

            // calculations

            Console.WriteLine(" \n Result ");
            Console.WriteLine(" ----------------------- ");

            switch (operation)
            {
                case '+':
                    Console.WriteLine(" \n" + a + " + " + b + " = " + (a + b));
                    break;
                case '-':
                    Console.WriteLine(" \n" + a + " - " + b + " = " + (a - b));
                    break;
                case '*':
                    Console.WriteLine(" \n" + a + " * " + b + " = " + (a * b));
                    break;
                case '/':
                    Console.WriteLine(" \n" + a + " / " + b + " = " + (a / b));
                    break;
                case '%':
                    Console.WriteLine(" \n" + a + " % " + b + " = " + (a % b));
                    break;
                case '^':
                    Console.WriteLine(" \n" + a + " ^ " + b + " = " + (Math.Pow(a, b)));
                    break;
                default:
                    Console.WriteLine(" \n Error. " + operation + " - unknown operator.");
                    break;
            }

            Console.ReadKey();
        }
    }
}
