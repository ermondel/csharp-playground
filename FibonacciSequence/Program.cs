﻿using System;

namespace FibonacciSequence
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" \n Program - The Fibonacci Sequence, v0.1 \n");

            First10Numbers();

            Console.WriteLine("\n");

            Console.WriteLine(" 3: " + Fibonacci(3));
            Console.WriteLine(" 8: " + Fibonacci(8));
            Console.WriteLine(" 10: " + Fibonacci(10));

            Console.ReadKey();
        }

        static void First10Numbers()
        {
            int a = 0;
            int b = 1;
            int sum;

            Console.WriteLine(" 1");

            for (int i = 0; i < 9; i++)
            {
                sum = a + b;

                a = b;
                b = sum;

                Console.WriteLine(" " + b);
            }
        } 

        static int Fibonacci(int number)
        {
            if (number <= 1)
                return number;
            else 
                return Fibonacci(number - 1) + Fibonacci(number - 2);
        }
    }
}
