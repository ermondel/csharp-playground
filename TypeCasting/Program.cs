﻿using System;

namespace TypeCasting
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n Program - Type Casting v.0");

            double a = 1.0 + 1 + 1.0f;

            int x = (int)(7 + 3.0 / 4.0 * 2);

            int intMax = int.MaxValue;
            float floatMax = float.MaxValue;

            short a1 = 30000;
            short b1 = 30000;

            short sum = (short)(a1 + b1);

            Console.WriteLine("\n Results:");
            Console.WriteLine(" " + a);
            Console.WriteLine(" " + x);
            Console.WriteLine(" " + ((1 + 1) / 2 * 3));
            Console.WriteLine(" " + (1 / 2));
            Console.WriteLine(" " + intMax);
            Console.WriteLine(" " + floatMax);
            Console.WriteLine(" " + sum);
        }
    }
}
