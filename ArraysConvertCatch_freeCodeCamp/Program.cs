﻿using System;

namespace Lesson3003
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // go1();
            // go2();
            go3();

            Console.ReadKey();
        }

        static void go3()
        {
            try
            {
                Console.Write("Enter X value: ");
                int num1 = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter Y value: ");
                int num2 = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(num1 / num2);
            } catch (DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
            } catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void go2()
        {
            int[,] numberGrid =
            {
                {1, 2},
                {3, 4},
                {5, 6}
            };

            int[,] myArray = new int[2, 3];

            myArray[1, 1] = 777;

            Console.WriteLine(numberGrid[2, 1]);
            Console.WriteLine(myArray[1, 1]);
        }

        static void go1()
        {
            for (int n = 1; n <= 10; n++)
                Console.WriteLine(GetPow(2, n));
        }

        static int GetPow(int baseNum, int powNum)
        {
            int result = 1;

            for (int i = 0; i < powNum; i++)
                result *= baseNum;

            return result;
        }
    }
}
