# C# Playground

## Создание и настройка проекта через CLI на примере установки пакетов тестирования

### Шаг 1

Находясь в командной строке в корне ./Git выполнить  

`dotnet new sln -o myproject`  

Это создаст папку под названием **myproject** в которой будет находится файл **myproject.sln**.  
Это будет *Решение*, которое в C# служит основной будущей структуры, и внутри этого решения будут размещатся все остальные *проекты*: программы, библиотеки, тестирование и т.д.

### Шаг 2

`cd myproject`  

`mkdir myproject`  

Перейдем в наше решение и создадим там одноименную папку **myproject**, которая будет будущей программой.

### Шаг 3

`cd myproject`  

`dotnet new console`  

Перейдем в эту папку для будущей программы и создадим саму программу, например это будет консольное приложение.

### Шаг 4

`cd ..`  

`dotnet nuget list source`  

Поднимемся выше и проверим список ресурсом откуда будут поступать пакеты. Там должно быть два - локальный на компьютере и удаленный сайт репозиторий пакетов (оттуда будут скачаны те пакеты, которых не будет локально). Если список пуст то нужно будет их добавить в менджер nuget **через** визуальный интерфейс IDE Visual Studio.

Каким должен быть этот список

```
Зарегистрированные источники:  
  1.  Microsoft Visual Studio Offline Packages [Включено]  
      C:\Program Files (x86)\Microsoft SDKs\NuGetPackages\  
  2.  nuget.org [Включено]  
      https://api.nuget.org/v3/index.json  
```

### Шаг 5

`dotnet list package`

Проверим установленные пакеты для проектов решения. Сейчас он пока будет пуст.

### Шаг 6

`dotnet sln add .\myproject\myproject.csproj`

Добавим в файл решения информацию о нашем консольном проекте (программе). Если этого не сделать, решение **не будет** догадываться о существовании проекта (даже если он в подпапках раположен в файловой системе).

### Шаг 7

`dotnet list package`

Проверим установленные пакеты для проектов решения. Теперь там уже будет один пункт с пустым списком пакетов - это будущие пакеты относящиеся к нашему проекту консольного приложения.

```
Проект "myproject" содержит следующие ссылки на пакеты
   [net5.0]: Пакеты для этой платформы не обнаружены.
```

### Шаг 8

`dotnet new classlib -o myproject.Tests`

Создадим проект для тестирования для примера в виде набора классов. Эта команда создаст подпапку **myproject.Tests** с файлами проекта внутри:

- obj *[папка]*
- Class1.cs *[это будет код тестирования]*
- myproject.Tests.csproj *[это файл проекта, и он нужен для файла решения в т.ч.]*

### Шаг 9

`dotnet sln add ./myproject.Tests/myproject.Tests.csproj`

Добавим в файл решения информацию о нашем тестировочном проекте (библиотеки). Если этого не сделать, решение **не будет** догадываться о существовании проекта (даже если он в подпапках раположен в файловой системе).

### Шаг 10

`dotnet list package`

Проверим установленные пакеты для проектов решения. Теперь там уже будет два пункта с пустым списком пакетов: 

1. Будущие пакеты относящиеся к нашему проекту консольного приложения;
2. Будущие пакеты относящиеся к нашему проекту тестирования.

```
Проект "myproject" содержит следующие ссылки на пакеты
   [net5.0]: Пакеты для этой платформы не обнаружены.
Проект "myproject.Tests" содержит следующие ссылки на пакеты
   [net5.0]: Пакеты для этой платформы не обнаружены.
```

### Шаг 11

`cd myproject.Tests`  
`dotnet add package Microsoft.NET.Test.Sdk`  
`dotnet add package xunit`  
`dotnet add package xunit.runner.visualstudio`  
`dotnet add package coverlet.collector`  
`cd ..`  

Перейдем в папку нашего проекта тестирования и там добавим пакеты необходимые для работы с xunit. После чего вернемся назад на уровень выше.

### Шаг 12

`dotnet list package`

Проверим установленные пакеты для проектов решения. Там будут такие пункты: 

1. Будущие пакеты относящиеся к нашему проекту консольного приложения (пустой список);
2. Установленные пакеты относящиеся к нашему проекту тестирования (4-ре пункта).

```
Проект "myproject" содержит следующие ссылки на пакеты
   [net5.0]: Пакеты для этой платформы не обнаружены.
Проект "myproject.Tests" содержит следующие ссылки на пакеты
   [net5.0]:
   Пакет верхнего уровня            Запрошено   Разрешено
   > coverlet.collector             3.1.2       3.1.2
   > Microsoft.NET.Test.Sdk         17.1.0      17.1.0
   > xunit                          2.4.1       2.4.1
   > xunit.runner.visualstudio      2.4.5       2.4.5
```

--------------------  

### Черновик комманд

```
dotnet add package xunit
dotnet restore
dotnet new xunit
dotnet new
dotnet nuget list source
dotnet nuget list
dotnet list package
dotnet new xunit -o Tests
dotnet add package Microsoft.NET.Test.Sdk
dotnet sln add .\mytest1.csproj
dotnet new console
dotnet new sln -o mytest1
dotnet new xunit -o PrimeService.Tests
dotnet sln add ./PrimeService/PrimeService.csproj
dotnet new sln -o unit-testing-using-dotnet-test
dotnet new classlib -o PrimeService
```

### Ссылки на литературу

- [Unit testing C# in .NET Core using dotnet test and xUnit](https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test)
